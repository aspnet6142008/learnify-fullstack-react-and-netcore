import React from "react";
import Courses from "../components/Courses";

function Homepage() {
  return (
    <div>
      <Courses />
    </div>
  );
}

export default Homepage;
