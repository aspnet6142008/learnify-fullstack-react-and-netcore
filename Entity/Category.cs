using System;
using System.Collections.Generic;

namespace Entity
{
    public class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }

        // one to many course relationship
        public ICollection<Course> Courses { get; set; }
    }
}