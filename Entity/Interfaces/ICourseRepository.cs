using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Entity.Interfaces
{
    public interface ICourseRepository // must adhere to ICourseRepository
    {
        // Task - represents async work to be done. Type of Course object for the result type
        Task<Course> GetCourseByIdAsync(Guid id); // return single course

        Task<IReadOnlyList<Course>> GetCoursesAsync(); // return all courses
    }
}

