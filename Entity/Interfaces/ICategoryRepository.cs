using System.Collections.Generic;
using System.Threading.Tasks;

namespace Entity.Interfaces
{
    public interface ICategoryRepository
    {
        Task<IReadOnlyList<Category>> GetCategoriesAsync(); // return all categories

        Task<Category> GetCategoryByIdAsync(int id); // return single category
    }
}