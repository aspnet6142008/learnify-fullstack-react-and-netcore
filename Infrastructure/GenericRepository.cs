using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entity;
using Entity.Interfaces;
// using Entity.Specifications;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class GenericRepository<T> : IGenericRepository<T>
    {
        private readonly StoreContext _context;
        public GenericRepository(StoreContext context)
        {
            _context = context;
        }

        public Task<IReadOnlyList<T>> ListAllAsync()
        {
            throw new System.NotImplementedException();
        }

        public async Task<T> GetByIdAsync(dynamic id)
        {
            throw new System.NotImplementedException();
            // return await _context.Set<T>().FindAsync(id);
        }

        // public async Task<T> GetEntityWithSpec(ISpecification<T> spec)
        // {
        //     return await ApplySpec(spec).FirstOrDefaultAsync();
        // }

        // public async Task<IReadOnlyList<T>> ListAllAsync()
        // {
        //     return await _context.Set<T>().ToListAsync();
        // }

        // public async Task<IReadOnlyList<T>> ListWithSpec(ISpecification<T> spec)
        // {
        //     return await ApplySpec(spec).ToListAsync();
        // }

        // private IQueryable<T> ApplySpec(ISpecification<T> spec)
        // {
        //     return SpecificationEvaluator<T>.GetQuery(_context.Set<T>().AsQueryable(), spec);
        // }
    }
}