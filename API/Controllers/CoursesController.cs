using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Dto;
using AutoMapper;
using Entity;
using Entity.Interfaces;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    public class CoursesController : BaseController
    {
        // api/courses
        // api/courses/id
        
        // private readonly StoreContext _context; // StoreContext is a database context for Entity Framework
        private readonly ICourseRepository _repository;

        private readonly IMapper _mapper;
        // Dependency injection of StoreContext & AutoMapper into the controller
        public CoursesController(ICourseRepository repository, IMapper mapper ) // (StoreContext context) 
        {
            _mapper = mapper;
            // abstract db via repository pattern instead
            _repository = repository;
            // _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<CourseDto>>> GetCourses()
        {
            var courses = await _repository.GetCoursesAsync();
            return Ok(_mapper.Map<IReadOnlyList<Course>, IReadOnlyList<CourseDto>>(courses));
            // return Ok(courses); // 200 response along with all courses
            // return await _context.Courses.ToListAsync(); // Retrieve all courses from the database
        }

        // HTTP GET method to get course by ID
        [HttpGet("{id}")]

        public async Task<ActionResult<CourseDto>> GetCourse(Guid id)
        {
            var course = await _repository.GetCourseByIdAsync(id);
            return _mapper.Map<Course, CourseDto>(course);
            // return await _repository.GetCourseByIdAsync(id);
            // return await _context.Courses.FindAsync(id); // Find a course by ID in the database
        }
    }
}